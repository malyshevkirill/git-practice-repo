// git-practice.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <algorithm>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const char *INPUT_FILE = "input.txt";
static const char *OUTPUT_FILE = "output.txt";

template <typename T>
struct functor
 {
    T _x;
    functor() : _x(1) {}
    functor (T x): _x(x) {}
    T operator() (T y)
    {
        return _x + y;
    }
};

template <typename T>
struct predicate
 {
    bool operator () (T & x1)
     {
        if(x1 > 0){
            std::cout << "more then 0" << std::endl;
            return true;
        }
        else{
            std::cout << "less then 0" << std::endl;
            return false;
        }
     }

 };

template <typename T>
struct printer
 {
     void operator()(T& e)
     {
        std::cout << e << std::endl;
     }
 };

template <typename T>
class input_reader
 {
 public:
    std::vector<T> operator()(const char* input)
     {
        int n;
        std::vector<T> source_v;
        std::ifstream file("input.txt");
        while (!file.eof())
        {
            file >> n;
            source_v.push_back(n);
            std::cout << n << std::endl;
        }
        file.close();
        return source_v;
     }
 };

template <typename T>
class output_writer
 {
 public:
    void operator()(const char* filename, const std::vector<T> vector_to_put_out)
     {
        std::ofstream output_file(filename, std::ofstream::trunc);
        for (unsigned int i = 0; i<vector_to_put_out.size(); i++)
        {
            output_file << vector_to_put_out[i] << std::endl;
        }
     }
 };

int _tmain(int argc, _TCHAR* argv[])
{
    std::vector<int> source_v;


    std::vector<int> target_v;

    input_reader<int> read;
    source_v = read(INPUT_FILE);

    predicate<int> pred;
    std::copy_if(source_v.begin(), source_v.end(), std::back_inserter(target_v), pred);

    functor<int> func;
    std::for_each(target_v.begin(), target_v.end(), func);

    printer<int> printr;
    std::for_each(target_v.begin(), target_v.end(), printr);

    output_writer<int> write;
    write(OUTPUT_FILE, target_v);

    return 0;
};